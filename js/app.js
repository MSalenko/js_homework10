const tabActions = document.querySelector('.tabs');
const tabContent = document.querySelector('.tabs-content');

// Додаємо дефолтні значення:
const defaultTab = document.querySelector('.active');
const defaultLi = document.querySelector(`.${defaultTab.dataset.tab}`);
for (const el of tabContent.children) {
  if(el !== defaultLi)
  el.style.display = 'none';
}

tabActions.addEventListener('click', (ev) => {
  for (const el of tabActions.children) {
    el.classList.remove('active');
  }
  ev.target.classList.add('active');

  const li = document.querySelector(`.${ev.target.dataset.tab}`);

  for (const el of tabContent.children) {
    el.style.display = 'none';
  }
  li.style.display = 'block';
});
